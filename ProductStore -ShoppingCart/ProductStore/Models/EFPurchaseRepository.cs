﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class EFPurchaseRepository : IPurchaseRepository
    {

        private ProductDBContext context;
        public IEnumerable<Purchase> Purchases => context.Purchases;

        public EFPurchaseRepository(ProductDBContext context)
        {
            this.context = context;
        }
    }
}
