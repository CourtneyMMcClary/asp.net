﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(ProductDBContext context)
        {

            if (!context.Users.Any())
            {
                context.Users.AddRange(

                    new User
                    {
                        FirstName = "Dana",
                        LastName = "Scully",
                        Email = "dscully@fbi.gov"

                    },
                    new User
                    {
                        FirstName = "Fox",
                        LastName = "Mulder",
                        Email = "fmulder@fbi.gov"

                    },
                    new User
                    {
                        FirstName = "Walter",
                        LastName = "Skinner",
                        Email = "wskinner@fbi.gov"

                    }

                );

                context.SaveChanges();
            }

            if (!context.Products.Any())
            {
                context.Products.AddRange(

                    new Product
                    {
                        ProductName = "T-Shirt",
                        Description = "Unisex shirt",
                        Category = "Tri-blend cotton",
                        Price = 14.75m
                    },
                    new Product
                    {
                        ProductName = "Men's sporty sweatshirt",
                        Description = "Warm and sporty",
                        Category = "Polyester",
                        Price = 59.99m
                    },
                    new Product
                    {
                        ProductName = "Child's Long Sleeve",
                        Description = "Unisex children's long sleeve shirt",
                        Category = "Tri-blend cotton",
                        Price = 19.50m
                    },
                    new Product
                    {
                        ProductName = "Women's blouse",
                        Description = "Dressy and fashionable shirt for women",
                        Category = "Polyester/Cotton",
                        Price = 34.95m
                    },
                    new Product
                    {
                        ProductName = "Graphic T-shirt",
                        Description = "Fashionable t-shirt",
                        Category = "Tri-blend cotton",
                        Price = 21.45m
                    },
                    new Product
                    {
                        ProductName = "Men's button down shirt",
                        Description = "Dressy button down shirt for any business casual event",
                        Category = "Polyester/Cotton",
                        Price = 25.99m
                    },
                    new Product
                    {
                        ProductName = "Women's Spandex running tank top",
                        Description = "Sporty and fashionable shirt for working out",
                        Category = "Polyester",
                        Price = 29.95m
                    }

                );

                context.SaveChanges();
            }


            //if (!context.Purchases.Any())
            //{
            //    context.Purchases.AddRange(

            //        new Purchase
            //        {

            //            AmountSpent = 14.75m
            //        },
            //        new Purchase
            //        {
            //            AmountSpent = 59.99m
            //        },
            //        new Purchase
            //        {
            //            AmountSpent = 19.50m
            //        }

            //    );
            //    context.SaveChanges();
            //}


        }
        }
    }

