﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class EFProductRepository : IProductRepository
    {

        private ProductDBContext context;
        public IEnumerable<Product> Products => context.Products
            .Include(p => p.PurchaseProducts)
            .ThenInclude(pp => pp.Purchase)
            .ThenInclude(pu => pu.User);
            

        public EFProductRepository(ProductDBContext context)
        {
            this.context = context;
        }
    }
}
