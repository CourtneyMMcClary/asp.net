﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    [JsonObject(IsReference = true)]
    public class Purchase
    {
        public int PurchaseId { get; set; }

        //Associations
        public User User { get; set; }       
        public int UserId { get; set; }

        public List<PurchaseProduct> PurchaseProducts { get; set; }

    }

}


