﻿using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Components
{
    public class PurchaseSummaryViewComponent : ViewComponent
    {
        private Purchasing purchase;

        public PurchaseSummaryViewComponent(Purchasing purchase)
        {
            this.purchase = purchase;
        }

        public IViewComponentResult Invoke()
        {
            return View(purchase);
        }
    }
}
