﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;

namespace ProductStore.Controllers
{
    public class UserController : Controller
    {

        private IUserRepository userRepo;

        public UserController(IUserRepository userRepo)
        {
            this.userRepo = userRepo;
        }

        public IActionResult List()
        {
            return View(userRepo.Users);
                //.OrderBy(u => u.FirstName).ThenBy(u => u.LastName));
        }
    }
}