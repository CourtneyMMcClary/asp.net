﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;
using ProductStore.Models.ViewModels;

namespace ProductStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository productRepo;
        private int PageSize = 4;

        public ProductController(IProductRepository productRepo)
        {
            this.productRepo = productRepo;
        }

        public IActionResult List(string category, int pageNo = 1)
        {
            ProductListViewModel model = new ProductListViewModel();
            IEnumerable<Product> pagedProducts;
            if (category == null)
            {
                pagedProducts = productRepo.Products
                   .OrderBy(p => p.ProductId)
                   .Skip((pageNo - 1) * PageSize)
                   .Take(PageSize);
            }
            else
            {
                pagedProducts = productRepo.Products
                   .Where(p => p.Category == null || p.Category == category)
                   .OrderBy(p => p.ProductId)
                   .Skip((pageNo - 1) * PageSize)
                   .Take(PageSize);
            }

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    productRepo.Products
                    .Count() :
                     productRepo.Products
                    .Where(p => p.Category == category)
                    .Count()
            };

            model.Products = pagedProducts;


            //stuffing view with things from viewmodel
            //filters
            model.CurrentCategory = category;

            model.PagingInfo = pInfo;

            return View(model);
        }
    }
}