﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;
using ProductStore.Models.ViewModels;

namespace ProductStore.Controllers
{
    public class PurchasingController : Controller
    {
        private IProductRepository productRepo;
        private Purchasing purchase;

        public PurchasingController(IProductRepository productRepo, Purchasing purchase)
        {
            this.productRepo = productRepo;
            this.purchase = purchase;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new PurchaseIndexViewModel
            {
                Purchasing = purchase,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public RedirectToActionResult AddToCart(int productId, string returnUrl)
        {
            Product product = productRepo.Products.FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                purchase.AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnUrl)
        {
            Product product = productRepo.Products.FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                purchase.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

    }
}
