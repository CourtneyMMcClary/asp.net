﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    [JsonObject(IsReference = true)]
    public class PurchaseProduct
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseProductId { get; set; }
        public int NumberOfProducts { get; set; }
      

        //Associations
        public Product Product { get; set; }
        public int ProductId { get; set; }

        public Purchase Purchase { get; set; }
        public int PurchaseId { get; set; }

    }
}
