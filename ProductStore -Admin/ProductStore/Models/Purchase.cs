﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    [JsonObject(IsReference = true)]
    public class Purchase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [BindNever]
        public int PurchaseId { get; set; }

        [BindNever]
        public ICollection<PurchaseProduct> Items { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter your last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter street name")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Please enter your city")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter your state")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please enter your zip code")]
        public string Zip { get; set; }

        //[Required(ErrorMessage = "Please enter your country")]
        //public string Country { get; set; }


        [BindNever]
        public bool Shipped { get; set; }



        //Associations
        //public User User { get; set; }       
        //public int UserId { get; set; }

        //public List<PurchaseProduct> PurchaseProducts { get; set; }

    }

}


