﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProductStore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    
    public class SessionCart : Purchasing
    {      
        [JsonIgnore]
        public ISession Session { get; set; }

        public static Purchasing GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
           .HttpContext.Session;
            SessionCart cart = session?.GetJson<SessionCart>("Purchasing") ??
                new SessionCart();

            cart.Session = session;

            return cart;
        }

        public override void AddItem(Product product, int quantity)
        {
            base.AddItem(product, quantity);
            Session.SetJson("Purchasing", this);
        }

        public override void RemoveLine(Product product)
        {
            base.RemoveLine(product);
            Session.SetJson("Purchasing", this);
        }

        public override void Clear()
        {
            base.Clear();
            Session.Remove("Purchasing");
        }
    }
}
