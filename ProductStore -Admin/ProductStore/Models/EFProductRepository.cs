﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class EFProductRepository : IProductRepository
    {

        private ProductDBContext context;
        public IEnumerable<Product> Products => context.Products
            .Include(p => p.PurchaseProducts)
            .ThenInclude(pp => pp.Purchase)
            /*.ThenInclude(pu => pu.User)*/;


        public EFProductRepository(ProductDBContext context)
        {
            this.context = context;
        }

        public void SaveProduct(Product product)
        {
            if (product.ProductId == 0)
            {
                //add new product to DB
                context.Products.Add(product);

            }
            else
            {
                //update
                Product efProduct = context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);
                if (efProduct != null)
                {
                    efProduct.ProductName = product.ProductName;
                    efProduct.Description = product.Description;
                    efProduct.Price = product.Price;
                    efProduct.Category = product.Category;
                }
            }

            context.SaveChanges();
        }

        public void DeleteProduct(Product product)
        {
            Product pr = context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);

            if (pr != null)
            {
                context.Products.Remove(pr);
            }

            context.SaveChanges();
        }
    }
}
