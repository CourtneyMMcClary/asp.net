﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models.ViewModels
{
    public class PurchaseIndexViewModel
    {    
        public Purchasing Purchasing { get; set; }
        public string ReturnUrl { get; set; }
    }
}
