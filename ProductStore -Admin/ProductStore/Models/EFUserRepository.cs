﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class EFUserRepository : IUserRepository
    {

        private ProductDBContext context;
        public IEnumerable<User> Users => context.Users;

        public EFUserRepository(ProductDBContext context)
        {
            this.context = context;
        }
    }
}
