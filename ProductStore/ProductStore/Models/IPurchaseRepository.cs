﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public interface IPurchaseRepository
    {
        IEnumerable<Purchase> Purchases { get; }
    }
}
