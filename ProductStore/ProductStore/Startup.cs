﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductStore.Models;

namespace ProductStore
{
    public class Startup
    {
        IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json").Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //string conn = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            services.AddDbContext<ProductDBContext>(options =>
                //options.UseSqlServer(conn));
                options.UseSqlServer(Configuration["Data:ProductsStore:ConnectionString"]));

            services.AddTransient<IProductRepository, EFProductRepository>();
            services.AddTransient<IUserRepository, EFUserRepository>();
            services.AddTransient<IPurchaseRepository, EFPurchaseRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ProductDBContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(
               routes =>
               {
                   routes.MapRoute(
                     name: "pagination",
                     template: "Product/Page{pageNo}",
                     defaults: new { Controller = "Product", action = "List" }
                      );

                   routes.MapRoute(
                      name: "Default",
                      template: "{controller=Product}/{action=List}/{id?}"
                       );
               });

            app.UseStaticFiles();
            app.UseStatusCodePages();
           
        }
    }
}
