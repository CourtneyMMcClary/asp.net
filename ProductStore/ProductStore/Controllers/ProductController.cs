﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;
using ProductStore.Models.ViewModels;

namespace ProductStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository productRepo;
        private int PageSize = 4;

        public ProductController(IProductRepository productRepo)
        {
            this.productRepo = productRepo;
        }

        public IActionResult List(int pageNo = 1)
        {
            ProductListViewModel model = new ProductListViewModel();

            IEnumerable<Product> pagedProducts = productRepo.Products
                .OrderBy(p => p.ProductId)
                .Skip((pageNo - 1) * PageSize)
                .Take(PageSize);

            model.Products = pagedProducts;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = productRepo.Products.Count()
            };

            model.PagingInfo = pInfo;

            return View(model);
        }
    }
}