﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Razor.Models;

namespace Razor.Controllers
{
    public class HomeController : Controller
    {

        SimpleRepository repo = SimpleRepository.SharedRepo;

        public ViewResult Index()
        {

            IEnumerable<Product> frepo = repo.Products.
                //Where(p => p.Price < 50).
                OrderBy(p => p.Name);

            return View(frepo);
        }

        [HttpGet]
        public IActionResult AddProduct() => View(new Product());

        [HttpPost]
        public IActionResult AddProduct(Product p)
        {
            repo.AddProduct(p);
            return RedirectToAction("Index");
        }
    }
}