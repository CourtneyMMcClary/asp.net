using Razor.Models;
using System;
using Xunit;

namespace Razor.Tests
{
    public class ProductTest
    {
        [Fact]
        public void CanChangeProductName()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };

            //Act
            p.Name = "New Name";


            //Assert
            Assert.Equal("New Name", p.Name);
        }

        [Fact]
        public void CanChangeProductPrice()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };

            //Act
            p.Price = 200M;


            //Assert
            Assert.Equal(200M, p.Price);
        }
    }
}
