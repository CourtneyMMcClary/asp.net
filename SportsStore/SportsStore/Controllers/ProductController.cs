﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        private int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            prodRepo = repo;
        }

        public ViewResult List(int pageNo = 1)
        {
            ProductListViewModel model = new ProductListViewModel();

            IEnumerable<Product> pagedProducts = prodRepo.Products
                .OrderBy(p => p.ProductId)
                .Skip((pageNo - 1) * PageSize)
                .Take(PageSize);

            model.Products = pagedProducts;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = prodRepo.Products.Count()
            };

            model.PagingInfo = pInfo;

            return View(model);
        }
    }
}