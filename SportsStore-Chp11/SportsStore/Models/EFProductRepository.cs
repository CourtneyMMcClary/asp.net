﻿using System.Collections.Generic;
using System.Linq;

namespace SportsStore.Models
{
    public class EFProductRepository : IProductRepository
    {
        private ApplicationDbContext context;

        public EFProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Product> Products => context.Products;

        public void DeleteProduct(Product product)
        {
            Product pr = context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);

            if (pr != null)
            {
                context.Products.Remove(pr);
            }

            context.SaveChanges();
        }

        public void SaveProduct(Product product)
        {
            if (product.ProductId == 0)
            {
                //add new product to DB
                context.Products.Add(product);
                
            }
            else
            {
                //update
                Product efProduct = context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);
                if(efProduct != null)
                {
                    efProduct.Name = product.Name;
                    efProduct.Description = product.Description;
                    efProduct.Price = product.Price;
                    efProduct.Category = product.Category;
                }
            }

            context.SaveChanges();
        }
    }
}