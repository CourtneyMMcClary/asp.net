﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CdTracker.Models
{
    public static class CDRepo
    {
        //keeps track of the list of CD's
        public static List<CD> response = new List<CD>();

        public static IEnumerable<CD> Response
        {
            get
            {
                return response;
            }
        }

        public static void AddCd(CD responses)
        {
            response.Add(responses);
        }

    }
}
