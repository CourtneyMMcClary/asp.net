﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CdTracker.Models
{
    public class CD
    {
        DateTime dt = new DateTime();

        [Required(ErrorMessage = "Please enter your bank name")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Please enter a valid term")]
        public int Term { get; set; }

        [Required(ErrorMessage = "Please enter a rate")]
        public double Rate { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = "Purchase Date")]
        [DataType(DataType.Date)]
        public DateTime PurchaseDate { get; set; }
        
        public DateTime MaturityDate
        { get; set; }

        [Required(ErrorMessage = "Please enter your deposit amount")]
        public double DepositAmount { get; set; }

        
        public double ValueAtMaturity { get; set; }


    }
}
