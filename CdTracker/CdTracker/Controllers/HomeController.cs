﻿using CdTracker.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CdTracker.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        } 

        [HttpGet]
        public ViewResult AddCd()
        {
            return View();
        }


        [HttpPost]
        public ViewResult AddCd(CD resp)
        {
            if (ModelState.IsValid)
            {
                CDRepo.AddCd(resp);
                return View(resp);
            }
            else
            {
                return View(resp);
            }
        }

    }
}
