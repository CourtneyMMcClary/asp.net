﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XFiles.Models;

namespace XFiles.Components
{
    public class AgentSummaryViewComponent : ViewComponent
    {
        private CheckoutCase checkout;

        public AgentSummaryViewComponent(CheckoutCase checkout)
        {
            this.checkout = checkout;
        }

        public IViewComponentResult Invoke()
        {
            return View(checkout);
        }
    }
}
