﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XFiles.Models;

namespace XFiles.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private ICaseRepository caseRepo;

        public NavigationMenuViewComponent(ICaseRepository caseRepo)
        {
            this.caseRepo = caseRepo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];

            return View(
                caseRepo.Cases
                    .Select(p => p.Category)
                    .Distinct()
                    .OrderBy(p => p)
                );
        }
    }
}
