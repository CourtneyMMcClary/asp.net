﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class EFAgentRepository : IAgentRepository
    {
        private XFilesDBContext context;

        public IEnumerable<Agent> Agents => context.Agents
            .Include(o => o.Items)
            .ThenInclude(a => a.Case);

        public EFAgentRepository(XFilesDBContext context)
        {
            this.context = context;
        }

        public void SaveCheckout(Agent agent)
        {
            //gets all the lines in the order and saves them
            //don't resave the associated products
            context.AttachRange(agent.Items.Select(a=> a.Case));

            if (agent.AgentId == 0)
            {
                context.Agents.Add(agent);
            }

            //DONT FORGET TO SAVE CHANGES
            context.SaveChanges();
        }
    }
}
