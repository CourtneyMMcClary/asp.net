﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    //[JsonObject(IsReference = true)]
    public class AgentCase
    {
        public int AgentCaseId { get; set; }
        public int Quantity { get; set; }


        //Associations
        public Agent Agent { get; set; }
        public int AgentId { get; set; }

        public Case Case { get; set; }
        public int CaseId { get; set; }
    }
}
