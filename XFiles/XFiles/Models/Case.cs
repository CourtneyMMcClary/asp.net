﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    //[JsonObject(IsReference = true)]
    public class Case
    {     
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CaseId { get; set; }
        public string CaseName { get; set; }
        public string Location { get; set; }
        public string Report { get; set; }
        public string Category { get; set; }
        

        //Associations
        public List<AgentCase> AgentCases { get; set; }

        //public BadGuy BadGuys { get; set; }
        //public int BadGuyId { get; set; }
    }
}
