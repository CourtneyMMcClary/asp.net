﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class XFilesDBContext : DbContext
    {
        public XFilesDBContext(DbContextOptions<XFilesDBContext> options) : base(options)
        {

        }

        public DbSet<Agent> Agents { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<AgentCase> AgentCases { get; set; }
        public DbSet<BadGuy> BadGuys { get; set; }


    }
}
