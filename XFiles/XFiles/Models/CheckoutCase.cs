﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class CheckoutCase
    {
        private List<CaseLine> lineCollection = new List<CaseLine>();

        public virtual void AddCase(Case cases, int quantity)
        {
            CaseLine line = lineCollection.Where(p => p.Case.CaseId == cases.CaseId).FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CaseLine
                {
                    Case = cases,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public virtual void RemoveCase(Case cases) =>
            lineCollection.RemoveAll(l => l.Case.CaseId == cases.CaseId);

        //public virtual decimal ComputeTotalValue() =>
        //    lineCollection.Sum(e => e.Case. * e.Quantity);

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CaseLine> Lines => lineCollection;

    }

    public class CaseLine
    {
        public int CaseLineId { get; set; }
        public Case Case { get; set; }
        public int Quantity { get; set; }
    }
}

