﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XFiles.Infrastructure;

namespace XFiles.Models
{
    public class SessionCart : CheckoutCase
    {
        [JsonIgnore]
        public ISession Session { get; set; }

        public static CheckoutCase GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
           .HttpContext.Session;
            SessionCart cart = session?.GetJson<SessionCart>("CheckoutCase") ??
                new SessionCart();

            cart.Session = session;

            return cart;
        }

        public override void AddCase(Case cases, int quantity)
        {
            base.AddCase(cases, quantity);
            Session.SetJson("CheckoutCase", this);
        }

        public override void RemoveCase(Case cases)
        {
            base.RemoveCase(cases);
            Session.SetJson("CheckoutCase", this);
        }

        public override void Clear()
        {
            base.Clear();
            Session.Remove("CheckoutCase");
        }
    }
}
