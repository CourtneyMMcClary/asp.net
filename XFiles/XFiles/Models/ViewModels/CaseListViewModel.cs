﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models.ViewModels
{
    public class CaseListViewModel
    {
        public IEnumerable<Case> Cases { get; set; }
        public PagingInfo PagingInfo { get; set; }

        public string CurrentCategory { get; set; }

    }
}
