﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models.ViewModels
{
    public class AgentIndexViewModel
    {
        public CheckoutCase CheckoutCase { get; set; }
        public string ReturnUrl { get; set; }
    }
}
