﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class EFCaseRepository : ICaseRepository
    {
        private XFilesDBContext context;

        public EFCaseRepository(XFilesDBContext context)
        {
            this.context = context;
        }

        public IEnumerable<Case> Cases => context.Cases   
            .Include(c => c.AgentCases)
            .ThenInclude(cc => cc.Agent);

        public void DeleteCase(Case cases)
        {
            Case c = context.Cases.FirstOrDefault(cs => cs.CaseId == cases.CaseId);

            if (c != null)
            {
                context.Cases.Remove(c);
            }

            context.SaveChanges();
        }

        public void SaveCase(Case cases)
        {
            if (cases.CaseId == 0)
            {
                //add new product to DB
                context.Cases.Add(cases);

            }
            else
            {
                //update
                Case efCase = context.Cases.FirstOrDefault(cs=> cs.CaseId == cases.CaseId);
                if (efCase != null)
                {
                    efCase.CaseName = cases.CaseName;
                    efCase.Report = cases.Report;
                    efCase.Location = cases.Location;
                    efCase.Category = cases.Category;
                }
            }

            context.SaveChanges();
        }
    }
}
