﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public interface IBadGuyRepository
    {
        IEnumerable<BadGuy> BadGuys { get; }
    }
}
