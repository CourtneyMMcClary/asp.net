﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public interface IAgentRepository
    {
        IEnumerable<Agent> Agents { get; }
        void SaveCheckout(Agent agent);
    }
}
