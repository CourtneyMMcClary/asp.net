﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class EFBadGuyRepository : IBadGuyRepository
    {

        private XFilesDBContext context;
        public IEnumerable<BadGuy> BadGuys => context.BadGuys
            .Include(c=> c.Cases);

        public EFBadGuyRepository(XFilesDBContext context)
        {
            this.context = context;
        }
    }
}
