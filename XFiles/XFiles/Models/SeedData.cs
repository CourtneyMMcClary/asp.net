﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public class SeedData
    {
        public static void EnsurePopulated(XFilesDBContext context)
        {
            // ApplicationDbContext context = app.ApplicationServices
            //     .GetRequiredService<ApplicationDbContext>();

            //if (!context.BadGuys.Any())
            //{
            //    context.BadGuys.AddRange(

            //        new BadGuy
            //        {
            //            FirstName = "Dana",
            //            LastName = "Scully",
            //            Species = "Devil"

            //        },
            //        new BadGuy
            //        {
            //            FirstName = "Fox",
            //            LastName = "Mulder",
            //            Species = "Goblin"

            //        },
            //        new BadGuy
            //        {
            //            FirstName = "Walter",
            //            LastName = "Skinner",
            //            Species = "Mutant"

            //        }

            //    );

            //    context.SaveChanges();
            //}

            if (!context.Cases.Any())
            {
                context.Cases.AddRange(

                    new Case
                    {
                        CaseName = "Kayak",
                        Location = "A boat for one person",
                        Category = "Watersports",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Lifejacket",
                        Location = "Protective and fashionable",
                        Category = "Watersports",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Soccer Ball",
                        Location = "FIFA-approved size and weight",
                        Category = "Soccer",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Corner Flags",
                        Location = "Give your playing field a professional touch",
                        Category = "Soccer",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Stadium",
                        Location = "Flat-packed 35,000 stadium",
                        Category = "Soccer",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Thinking Cap",
                        Location = "Improve brain efficiency by 75%",
                        Category = "Chess",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Unsteady Chair",
                        Location = "Secretly give your opponent a disadvantage",
                        Category = "Chess",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Human Chess Board",
                        Location = "A fun game for the family",
                        Category = "Chess",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    },
                    new Case
                    {
                        CaseName = "Bling-Bling King",
                        Location = "Gold-plated, diamond-studded King",
                        Category = "Chess",
                        Report = "The FBI’s online vault features nine cases having to do with unexplained phenomena, including flying saucer reports, cattle mutilations, ESP and the purported Majestic-12 conspiracy."
                    }

                );

                context.SaveChanges();
            }

            if (!context.Agents.Any())
            {
                context.Agents.AddRange(

                    new Agent
                    {
                        FirstName = "Dana",
                        LastName = "Scully",
                        Age = "25",
                        Department = "XFiles",
                        BadgeNumber = "1456"

                    },
                    new Agent
                    {
                        FirstName = "Fox",
                        LastName = "Mulder",
                        Age = "28",
                        Department = "XFiles",
                        BadgeNumber = "1457"

                    },
                    new Agent
                    {
                        FirstName = "Walter",
                        LastName = "Skinner",
                        Age = "36",
                        Department = "FBI Assistant Director",
                        BadgeNumber = "145"

                    }

                );

                context.SaveChanges();
            }
        }
    }
}
