﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;

namespace XFiles.Models
{
    [JsonObject(IsReference = true)]
    public class Agent
    {    
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [BindNever]
        public int AgentId { get; set; }

        [BindNever]
        public ICollection<CaseLine> Items { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter your last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your age")]
        public string Age { get; set; }

        [Required(ErrorMessage = "Please enter your department")]
        public string Department { get; set; }

       [Required(ErrorMessage = "Please enter your badge number")]
        public string BadgeNumber { get; set; }


        [BindNever]
        public bool CheckedIn { get; set; }

        //Associations
        public List<AgentCase> AgentCases { get; set; }


    }
}
