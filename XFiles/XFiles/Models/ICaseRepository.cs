﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XFiles.Models
{
    public interface ICaseRepository
    {
       IEnumerable<Case> Cases { get; }
        void DeleteCase(Case c);
        void SaveCase(Case c);
    }
}
