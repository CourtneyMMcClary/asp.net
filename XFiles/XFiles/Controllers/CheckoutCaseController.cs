﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XFiles.Models;
using XFiles.Models.ViewModels;

namespace XFiles.Controllers
{
    public class CheckoutCaseController : Controller
    {
        private ICaseRepository caseRepo;
        private CheckoutCase checkout ;

        public CheckoutCaseController(ICaseRepository repository, CheckoutCase checkout)
        {
            this.caseRepo = repository;
            this.checkout = checkout;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new AgentIndexViewModel
            {
                CheckoutCase = checkout,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public RedirectToActionResult AddToCart(int caseId, string returnUrl)
        {
            Case cases = caseRepo.Cases.FirstOrDefault(c=> c.CaseId == caseId);

            if (cases != null)
            {
                checkout.AddCase(cases, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int caseId, string returnUrl)
        {
            Case cases = caseRepo.Cases.FirstOrDefault(c=> c.CaseId == caseId);

            if (cases != null)
            {
                checkout.RemoveCase(cases);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}