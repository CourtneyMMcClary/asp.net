﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using XFiles.Models;

namespace XFiles.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        private ICaseRepository caseRepo;


        public AdminController(ICaseRepository caseRepo)
        {
            this.caseRepo = caseRepo;
        }

        public IActionResult Index()
        {
            return View(caseRepo.Cases);
        }

        [HttpGet]
        public ViewResult Edit(int caseId)
        {
            if (caseId == 0)
            {
                return View(new Case());
            }
            else
            {
                return View(caseRepo.Cases.FirstOrDefault(c => c.CaseId == caseId));
            }
        }

        [HttpPost]
        public IActionResult Edit(Case cases)
        {
            if (ModelState.IsValid)
            {
                //save
                caseRepo.SaveCase(cases);
                TempData["message"] = $"{cases.CaseName} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                //somethings wrong with data
                return View(cases);
            }
        }

        public IActionResult Create()
        {
            return RedirectToAction("Edit", new { caseId = 0 });
        }

        [HttpPost]
        public ViewResult Delete(int caseId)
        {
            Case c = caseRepo.Cases.FirstOrDefault(p => p.CaseId == caseId);
            if (c != null)
            {
                TempData["message"] = $"{c.CaseName} has been deleted";
                caseRepo.DeleteCase(c);
            }

            return View("Index", caseRepo.Cases);
        }
    
    }
}