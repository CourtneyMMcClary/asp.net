﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XFiles.Models;
using XFiles.Models.ViewModels;

namespace XFiles.Controllers
{
    public class CaseController : Controller
    {
        private ICaseRepository caseRepo;
        private int PageSize = 4;

        public CaseController(ICaseRepository caseRepo)
        {
            this.caseRepo = caseRepo;
        }


        public IActionResult List(string category, int pageNo = 1)
        {
            CaseListViewModel model = new CaseListViewModel();
            IEnumerable<Case> pagedCases;
            if (category == null)
            {
                pagedCases = caseRepo.Cases
                   .OrderBy(p => p.CaseId)
                   .Skip((pageNo - 1) * PageSize)
                   .Take(PageSize);
            }
            else
            {
                pagedCases = caseRepo.Cases
                   .Where(p => p.Category == null || p.Category == category)
                   .OrderBy(p => p.CaseId)
                   .Skip((pageNo - 1) * PageSize)
                   .Take(PageSize);
            }

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    caseRepo.Cases
                    .Count() :
                     caseRepo.Cases
                    .Where(p => p.Category == category)
                    .Count()
            };

            model.Cases = pagedCases;


            //stuffing view with things from viewmodel
            //filters
            model.CurrentCategory = category;

            model.PagingInfo = pInfo;

            return View(model);
        }
    }
}