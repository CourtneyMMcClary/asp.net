﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using XFiles.Models;

namespace XFiles
{
    public class Startup
    {
        IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json").Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //services.AddMvc().AddJsonOptions(options => {
            //    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
            //    options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            //});

            //string conn = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=XFiles;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            services.AddDbContext<XFilesDBContext>(options =>
            //options.UseSqlServer(conn));
             options.UseSqlServer(Configuration["Data:XFiles:ConnectionString"]));

            services.AddTransient<ICaseRepository, EFCaseRepository>();
            services.AddTransient<IAgentRepository, EFAgentRepository>();
            services.AddTransient<IBadGuyRepository, EFBadGuyRepository>();

            services.AddScoped<CheckoutCase>(p => SessionCart.GetCart(p));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMemoryCache();
            services.AddSession();

            //services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<AppIdentityDbContext>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, XFilesDBContext context)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();

            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            //app.UseIdentity();


            app.UseMvc(
               routes =>
               {
                   routes.MapRoute(
                      name: "Error",
                      template: "Error",
                      defaults: new { Controller = "Error", action = "Error" }
                       );

                   routes.MapRoute(
                       name: null,
                       template: "{category}/Page{pageNo:int}",
                       defaults: new { Controller = "Case", action = "List" }
                        );

                   routes.MapRoute(
                     name: null,
                     template: "Page{pageNo:int}",
                     defaults: new { Controller = "Case", action = "List", pageNo = 1 }
                      );

                   routes.MapRoute(
                     name: null,
                     template: "{category}",
                     defaults: new { Controller = "Case", action = "List", pageNo = 1 }
                      );

                   routes.MapRoute(
                     name: null,
                     template: "",
                     defaults: new { Controller = "Case", action = "List", pageNo = 1 }
                      );

                   routes.MapRoute(name: null, template: "{controller}/{action}/{id?}");
               });

           SeedData.EnsurePopulated(context);

        }
    }
}
