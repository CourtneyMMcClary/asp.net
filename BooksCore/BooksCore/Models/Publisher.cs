﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public class Publisher
    {

        public int PublisherId { get; set; }
        public string PublisherName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        //Associations
        public List<Title> Titles { get; set; }


    }
}
