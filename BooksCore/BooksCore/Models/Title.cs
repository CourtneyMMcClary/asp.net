﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public class Title
    {

        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string Type { get; set; }
        public int? Pages { get; set; }
        [Column(TypeName="Money")]
        public decimal? Price { get; set; }
        public int? Sales { get; set; }
        public DateTime? PubDate { get; set; }
        public bool Contract { get; set; }

        //Association
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public List<TitleAuthor> TitleAuthors { get; set; }

    }
}
