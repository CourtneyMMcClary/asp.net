﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BooksCore.Models
{
    public class EFAuthorRepository : IAuthorRepository
    {
        //implementation of iauthorrepo
        private BookDBContext context;
        public IEnumerable<Author> Authors => context.Authors
            .Include(a => a.TitleAuthors)
            .ThenInclude(ta => ta.Title)
            .ThenInclude(p => p.Publisher);

        public EFAuthorRepository(BookDBContext context)
        {
            this.context = context;
        }
    }
}
