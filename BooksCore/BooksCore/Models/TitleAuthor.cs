﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public class TitleAuthor
    {

        public int TitleAuthorId { get; set; }
        public int AuthorOrder { get; set; }


        //Associations
        public Author Author { get; set; }
        public int AuthorId { get; set; }

        public Title Title { get; set; }
        public int TitleId { get; set; }
    }
}
