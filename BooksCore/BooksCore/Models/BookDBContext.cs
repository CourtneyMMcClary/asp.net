﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public class BookDBContext : DbContext
    {

        public BookDBContext(DbContextOptions<BookDBContext>options) : base(options)
        {

        }


        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<TitleAuthor> TitleAuthors { get; set; }


    }
}
