﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public interface IAuthorRepository
    {
        IEnumerable<Author> Authors { get; }
    }
}
