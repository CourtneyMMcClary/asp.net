﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksCore.Models
{
    public class EFTitleRepository : ITitleRepository
    {
        //implementation of interface ITitleRepo

        private BookDBContext context;

        public IEnumerable<Title> Titles => context.Titles;

        public EFTitleRepository(BookDBContext context)
        {
            this.context = context;
        }
    }
}
