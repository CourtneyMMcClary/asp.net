﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BooksCore.Models;

namespace BooksCore.Controllers
{
    public class AuthorController : Controller
    {
        private IAuthorRepository authorRepo;

        public AuthorController(IAuthorRepository authorRepo)
        {
            this.authorRepo = authorRepo;
        }

        public IActionResult List()
        {
            return View(authorRepo.Authors.OrderBy(a => a.FirstName).ThenBy(a => a.LastName));
        }
    }
}