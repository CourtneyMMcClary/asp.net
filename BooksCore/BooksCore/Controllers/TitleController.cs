﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace BooksCore.Controllers
{
    public class TitleController : Controller
    {
        private ITitleRepository titleRepo;

        public TitleController(ITitleRepository titleRepo)
        {
            this.titleRepo = titleRepo;
        }

        public IActionResult List()
        {
            return View(titleRepo.Titles.Where(t => t.Sales > 100000 && t.Price > 10).OrderByDescending(t => t.Sales));
        }
    }
}