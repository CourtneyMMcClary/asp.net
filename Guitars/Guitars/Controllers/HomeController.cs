﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Guitars.Models;
using Microsoft.AspNetCore.Mvc;

namespace Guitars.Controllers
{
    public class HomeController : Controller
    {

        GuitarRepo repo = GuitarRepo.SharedRepo;

        public ViewResult Index()
        {
            IEnumerable<Guitar> grepo = repo.Guitars.OrderBy(g => g.Name);

            return View(grepo);
        }


        [HttpGet]
        public IActionResult AddGuitar() => View(new Guitar());

        [HttpPost]
        public IActionResult AddGuitar(Guitar g)
        {
            repo.AddGuitar(g);
            return RedirectToAction("Index");
        }
    }
}