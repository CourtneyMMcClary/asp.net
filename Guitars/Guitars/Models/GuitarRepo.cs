﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Guitars.Models
{
    public class GuitarRepo
    {
        private static GuitarRepo sharedRepo = new GuitarRepo();
        private Dictionary<string, Guitar> guitars = new Dictionary<string, Guitar>();
        public IEnumerable<Guitar> Guitars => guitars.Values;
        public static GuitarRepo SharedRepo => sharedRepo;


        public GuitarRepo()
        {
            Guitar[] guitars = new Guitar[]
            {
                new Guitar{Name = "Les Paul", Price = 750M, Description = "Electric guitar", Category = "Electric", StockLevel=0},
                new Guitar{Name = "Fender Steel String", Price = 1100M, Description = "Acoustic guitar", Category = "Acoustic", StockLevel = 5},
                new Guitar{Name = "Gibson", Price = 1500M, Description = "Electric guitar", Category = "Electric", StockLevel= 8},
                new Guitar{Name = "Squier", Price = 400M, Description = "Bass guitar", Category = "Bass", StockLevel = 3}
            };

            foreach(Guitar g in guitars)
            {
                AddGuitar(g);
            }
           

        }

        public void AddGuitar(Guitar g) => guitars.Add(g.Name, g);
    
    }
}


