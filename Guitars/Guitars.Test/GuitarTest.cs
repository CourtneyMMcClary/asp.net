using Guitars.Models;
using System;
using Xunit;

namespace Guitars.Test
{
    public class GuitarTest
    {
       [Fact]
       public void HasLowStock()
        {
            //arrange
            Guitar g = new Guitar { StockLevel = 3 };

            //act
            g.StockLevel = 2;

            //assert
            Assert.Equal(2, g.StockLevel);
        }

        [Fact]
        public void VerifyPrice()
        {
            //arrange
            Guitar g = new Guitar { Price = 750M };

            //act
            g.Price = 550M;

            //assert
            Assert.Equal(550M, g.Price);
        } 
    }
}
