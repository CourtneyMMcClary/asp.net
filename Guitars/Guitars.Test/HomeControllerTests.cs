﻿using Guitars.Controllers;
using Guitars.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Guitars.Test
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexActionIsComplete()
        {
            HomeController controller = new HomeController();

            //actual
            ViewResult result = (ViewResult)controller.Index();
            IEnumerable<Guitar> modelGuitars = (IEnumerable<Guitar>)result?.ViewData.Model;

            //expected
            IEnumerable<Guitar> repoGuitar =
                GuitarRepo.SharedRepo.Guitars.OrderBy(g => g.Name);

            Assert.Equal(repoGuitar, modelGuitars,
                   Comparer.Get<Guitar>((g1, g2) =>
                                g1.StockLevel == g2.StockLevel));
        }
    }
}
