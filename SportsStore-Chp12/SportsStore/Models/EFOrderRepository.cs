﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class EFOrderRepository : IOrderRepository
    {
        private ApplicationDbContext context;

        public EFOrderRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Order> Orders => context.Orders
            .Include(o => o.Lines)
            .ThenInclude(l => l.Product);

        public void SaveOrder(Order order)
        {
            //gets all the lines in the order and saves them
            //don't resave the associated products
            context.AttachRange(order.Lines.Select(l => l.Product));

            if(order.OrderId == 0)
            {
                context.Orders.Add(order);
            }

            //DONT FORGET TO SAVE CHANGES
            context.SaveChanges();
        }
    }
}
