﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        private int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            prodRepo = repo;
        }

        public ViewResult List(string category, int pageNo = 1)
        {
            ProductListViewModel model = new ProductListViewModel();
            IEnumerable<Product> pagedProducts;
            if(category == null)
            {
                 pagedProducts = prodRepo.Products                   
                    .OrderBy(p => p.ProductId)
                    .Skip((pageNo - 1) * PageSize)
                    .Take(PageSize);
            }
            else
            {
                pagedProducts = prodRepo.Products
                   .Where(p => p.Category == null || p.Category == category)
                   .OrderBy(p => p.ProductId)
                   .Skip((pageNo - 1) * PageSize)
                   .Take(PageSize);
            }

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    prodRepo.Products                   
                    .Count() :
                     prodRepo.Products
                    .Where(p => p.Category == category)
                    .Count()
            };

            model.Products = pagedProducts;
           

            //stuffing view with things from viewmodel
            //filters
            model.CurrentCategory = category;

            model.PagingInfo = pInfo;

            return View(model);
        }
    }
}