﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;

namespace SportsStore.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        private IProductRepository productRepo;

        
        public AdminController(IProductRepository productRepo)
        {
            this.productRepo = productRepo;
        }

        public IActionResult Index()
        {   
            return View(productRepo.Products);
        }

        [HttpGet]
        public ViewResult Edit(int productId)
        {
            if (productId == 0)
            {
                return View(new Product());
            }
            else
            {
                return View(productRepo.Products.FirstOrDefault(p => p.ProductId == productId));
            }
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                //save
                productRepo.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                //somethings wrong with data
                return View(product);
            }       
        }        
       
        public IActionResult Create()
        {
            return RedirectToAction("Edit", new { productId = 0 });
        }

        [HttpPost]
        public ViewResult Delete(int productId)
        {
            Product pr = productRepo.Products.FirstOrDefault(p => p.ProductId == productId);
            if (pr != null)
            {
                TempData["message"] = $"{pr.Name} has been deleted";
                productRepo.DeleteProduct(pr);
            }

            return View("Index", productRepo.Products);
        }
    }
}