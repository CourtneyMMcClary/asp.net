﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {

        private IProductRepository prodRepo;

        public NavigationMenuViewComponent(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];

            return View(
                prodRepo.Products
                    .Select(p => p.Category)
                    .Distinct()
                    .OrderBy(p => p)
                );
        }
    }
}
