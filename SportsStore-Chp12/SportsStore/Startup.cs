﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SportsStore.Models;

namespace SportsStore
{
    public class Startup
    {
        IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json")
                .Build();
            //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();

            services.AddDbContext<ApplicationDbContext>(
                options =>             
                options.UseSqlServer(Configuration["Data:SportsStoreProducts:ConnectionString"]));

            services.AddDbContext<AppIdentityDbContext>(
             options =>
             options.UseSqlServer(Configuration["Data:SportsStoreIdentity:ConnectionString"]));

            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<AppIdentityDbContext>();

            services.AddTransient<IProductRepository, EFProductRepository>();
            services.AddTransient<IOrderRepository, EFOrderRepository>();

            //setting up cart service
            services.AddScoped<Cart>(sp => SessionCart.GetCart(sp));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
                
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseSession();
            app.UseStaticFiles();
            app.UseIdentity();
           // app.UseAuthentication();
            
            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                      name: "Error",
                      template: "Error",
                      defaults: new { Controller = "Error", action = "Error" }
                       );

                    routes.MapRoute(
                      name: null,
                      template: "{category}/Page{pageNo:int}",
                      defaults: new { Controller = "Product", action = "List" }
                       );

                    routes.MapRoute(
                      name: null,
                      template: "Page{pageNo:int}",
                      defaults: new { Controller = "Product", action = "List", pageNo = 1 }
                       );

                    routes.MapRoute(
                      name: null,
                      template: "{category}",
                      defaults: new { Controller = "Product", action = "List", pageNo = 1 }
                       );

                    routes.MapRoute(
                      name: null,
                      template: "",
                      defaults: new { Controller = "Product", action = "List", pageNo = 1}
                       );

                    routes.MapRoute(name: null, template: "{controller}/{action}/{id?}");

                });

            SeedData.EnsurePopulated(context);
        }
    }
}
