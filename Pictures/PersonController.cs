﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Words.Models;
using System.IO;
using Words.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Words.Controllers
{
    [Authorize]
    public class PersonController : Controller
    {
        private IPersonRepository repo;

        public PersonController(IPersonRepository repo)
        {
            this.repo = repo;
        }

        public ViewResult List()
        {
            return View(repo.People.OrderBy(p => p.LastName));
        }

        [HttpGet]
        public ViewResult Edit(int personId = 0)
        {
            //check if the person exists; otherwise, return new...
            Person p = repo.People.FirstOrDefault(b => b.PersonId == personId) ?? new Person();

            return View(new PersonEditViewModel
            {
                Person = p
            });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PersonEditViewModel personVM)
        {
            if (ModelState.IsValid)
            {
                if (personVM.Picture != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await personVM.Picture.CopyToAsync(memoryStream);
                        personVM.Person.Picture = memoryStream.ToArray();
                    }
                    //set the content type for the pic
                    personVM.Person.PictureContentType = personVM.Picture.ContentType;
                }
                
                repo.SavePerson(personVM.Person);
                TempData["message"] = $"{personVM.Person.FirstName} {personVM.Person.LastName} has been saved";
                return RedirectToAction("List");
            }
            else
            {
                return View(personVM);
            }
        }

        [HttpGet]
        public FileStreamResult ViewImage(int personId)
        {
            Person person = repo.People.FirstOrDefault(p => p.PersonId == personId);

            if (person == null || person.Picture == null)
            {
                return null;
            }

            MemoryStream ms = new MemoryStream(person.Picture);
            return new FileStreamResult(ms, "image / jpeg");            
        }

        public IActionResult Delete(int personId)
        {
            Person person = repo.People.FirstOrDefault(p => p.PersonId == personId);

            if (person != null)
            {
                repo.DeletePerson(person);
            }

            return RedirectToAction("List");
        }
    }
}