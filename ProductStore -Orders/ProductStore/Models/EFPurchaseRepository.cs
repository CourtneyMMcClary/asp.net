﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class EFPurchaseRepository : IPurchaseRepository
    {

        private ProductDBContext context;

        public EFPurchaseRepository(ProductDBContext context)
        {
            this.context = context;
        }

        public IEnumerable<Purchase> Purchases => context.Purchases
           .Include(o => o.Items)
           .ThenInclude(l => l.Product);

        public void SavePurchase(Purchase purchase)
        {
            context.AttachRange(purchase.Items.Select(i => i.Product));

            if (purchase.PurchaseId == 0)
            {
               
                context.Purchases.Add(purchase);
            }

            context.SaveChanges();
        }
    }
}
