﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    [JsonObject(IsReference = true)]
    public class Purchasing
    {
        private List<PurchaseProduct> purchaseItem = new List<PurchaseProduct>();

        public virtual void AddItem(Product product, int quantity)
        {
            PurchaseProduct item = purchaseItem.Where(p => p.Product.ProductId == product.ProductId).FirstOrDefault();

            if (item == null)
            {
                purchaseItem.Add(new PurchaseProduct
                {
                    Product = product,
                    NumberOfProducts = quantity
                });
            }
            else
            {
                item.NumberOfProducts += quantity;
            }
        }

        public virtual void RemoveLine(Product prod)
        {
            purchaseItem.RemoveAll(i => i.Product.ProductId == prod.ProductId);
        }

        public virtual decimal ComputeTotalValue() =>
              purchaseItem.Sum(e => e.Product.Price * e.NumberOfProducts);

        public virtual void Clear()
        {
            purchaseItem.Clear();
        }
        
        public virtual IEnumerable<PurchaseProduct> Items => purchaseItem;

    }
}
