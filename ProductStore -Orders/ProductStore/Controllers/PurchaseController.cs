﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductStore.Models;
using ProductStore.Models.ViewModels;

namespace ProductStore.Controllers
{
    public class PurchaseController : Controller
    {
        private IPurchaseRepository purchaseRepo;
        private Purchasing purchasing;

        public PurchaseController(IPurchaseRepository purRepo, Purchasing purchasing)
        {
            this.purchaseRepo = purRepo;
            this.purchasing = purchasing;
        }

        public ViewResult Checkout() => View(new Purchase());

        [HttpPost]
        public IActionResult Checkout(Purchase purchase)
        {
            if (purchasing.Items.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                purchase.Items = purchasing.Items.ToArray();
                purchaseRepo.SavePurchase(purchase);
                return RedirectToAction(nameof(Complete));
            }
            else
            {
                return View(purchase);
            }
        }

        public ViewResult Complete()
        {
            purchasing.Clear();
            return View();
        }

        public ViewResult Index()
        {
            return View(purchaseRepo.Purchases.Where(o => !o.Shipped));
        }
        public IActionResult MarkShipped(int purchaseId)
        {
            Purchase purchase = purchaseRepo.Purchases.FirstOrDefault(o => o.PurchaseId == purchaseId);
            if (purchase != null)
            {
                purchase.Shipped = true;
                purchaseRepo.SavePurchase(purchase);
            }
            return RedirectToAction("Index");
        }
    }
}