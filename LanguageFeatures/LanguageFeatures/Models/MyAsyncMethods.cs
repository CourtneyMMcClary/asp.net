﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public class MyAsyncMethods
    {

        //requesting contents of the Apress home page asynchronously as a Task 
        //public static Task<long?> GetPageLength()
        //{
        //    HttpClient client = new HttpClient();

        //    var httpTask = client.GetAsync("http://apress.com");

        //    //first return specifies that we are returning Task<http...> object
        //    return httpTask.ContinueWith((Task<HttpResponseMessage> antecedent) =>
        //    {
        //        return antecedent.Result.Content.Headers.ContentLength;
        //    });
        //}

        //keyword await used when calling the asynchronous method
        //async keyword must be added for method sig and used with await
        public async static Task<long?> GetPageLength()
        {
            HttpClient client = new HttpClient();

            var httpMessage = await client.GetAsync("http://apress.com");

            return httpMessage.Content.Headers.ContentLength;
        }

    }
}
