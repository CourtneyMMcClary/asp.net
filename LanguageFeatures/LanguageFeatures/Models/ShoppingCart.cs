﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public sealed class ShoppingCart : IEnumerable<Product>
    {
        //only thing that can go into this is a product
        public IEnumerable<Product> Products { get; set; }

        //returning an IEnumerable in it; allows us to use foreach loop
        public IEnumerator<Product> GetEnumerator()
        {
            return Products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //public decimal CartTotal { get; set; }

        ////method for getting cart total
        //public decimal GetCartTotal()
        //{
        //    CartTotal = 0;
        //    foreach (Product p in Products)
        //    {
        //        CartTotal += p?.Price ?? 0;
        //    }

        //    return CartTotal;
        //}
    }
}
