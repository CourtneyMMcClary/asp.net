﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public class Product
    {
        public string Name { get; set; }
        public decimal? Price { get; set; }
        //association
        public Product Related { get; set; }
        //shortcut to set property
        public string Category { get; set; } = "Watersports";
        //defaulting to true; not allowed to set property
        public bool InStock { get; } = true;

        //creating a getter that is read-only to get info passed in
        public bool NameBeginsWith => Name[0] == 'S';

        public Product(bool stock = true)   
        {
            InStock = stock;
        }

        public static Product[] GetProduct()
        {
            //creates products on the fly; shorthand so you don't have to use a constructor
            //object "initializer"
            //use them with collections
            Product kayak = new Product()
            {
                Name = "Kayak",
                Price = 275M
            };

            Product lifejacket = new Product(false)
            {
                Name = "Lifejacket",
                Price = 48.95M
            };

            kayak.Related = lifejacket;
            

            return new Product[] { kayak, lifejacket, null };

        }

    }
}
