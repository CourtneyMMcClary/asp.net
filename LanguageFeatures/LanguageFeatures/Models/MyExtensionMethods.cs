﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public static class MyExtensionMethods
    {



        //says that this is an extension method
        //dont need this method; bottom one does the same thing but accepts more 
        public static decimal TotalPrices(this ShoppingCart cartParam)
        {
            decimal total = 0;

            foreach(Product p in cartParam.Products)
            {
                total += p?.Price ?? 0;
            }

            return total;
        }

        //more generic to have this method. 
        //this method accepts more data types than the above one allowing it to be more generic
        public static decimal TotalPrices(this IEnumerable<Product> products)
        {

            decimal total = 0;

            foreach (Product p in products)
            {
                total += p?.Price ?? 0;
            }

            return total;

        }

        public static IEnumerable<Product> FilterByPrice(this IEnumerable<Product> products, decimal minPrice)
        {
            //List<Product> filtProducts = new List<Product>();

            foreach(Product p in products)
            {
                if(p?.Price > minPrice)
                {
                    //filtProducts.Add(p);


                    //shorthand for adding up things in loops 
                    yield return p;
                }
            }

           // return filtProducts;
        }

        public static IEnumerable<Product> FilterByName(this IEnumerable<Product> products, char firstChar)
        {
            foreach (Product p in products)
            {
                if (p?.Name?[0] == firstChar)
                {
                    yield return p;
                }
            }

        }

        public static IEnumerable<Product> Filter(this IEnumerable<Product> products, Func<Product, bool> selector)
        {
            foreach (Product p in products)
            {
                if (selector(p))
                {
                    yield return p;
                }
            }

        }




    }
}
