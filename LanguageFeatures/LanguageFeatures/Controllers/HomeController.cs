﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;

namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {

        bool FilterByPrice(Product p)
        {
            return (p?.Price ?? 0) >= 20;

        }

        /*
        public ViewResult Index()
        {
            return View(new string[] { "C#", "Language", "Features" });
        }
        */


        //public ViewResult Index()
        //{
        //    List<string> results = new List<string>();

        //    foreach (Product p in Product.GetProduct())
        //    {
        //        string name = p?.Name ?? "<No Name>";
        //        decimal? price = p?.Price ?? 0;


        //        /*
        //        results.Add(
        //            string.Format("Name: {0}, Price: {1}, Related: {2}",
        //            name, price, p?.Related?.Name));
        //        */

        //        results.Add($"Name: {name}, Price: {price:C2}, Related: {p?.Related?.Name ?? "<No relation>"}");
        //    }

        //    return View(results);
        //}

        //public ViewResult Index()
        //{

        //    //faster than collection, only takes one step
        //    Dictionary<string, Product> products =
        //        new Dictionary<string, Product>()
        //        {
        //            { "Kayak", new Product(){ Name = "Kayak", Price = 275M } },
        //            { "Lifejacket", new Product(){ Name = "Lifejacket", Price = 48.95M } }
        //        };

        //    //Product p = products["Kayak"];


        //    //Dictionary<string, Product> products =
        //    //   new Dictionary<string, Product>
        //    //   {
        //    //        { "Kayak"} = new Product { Name = "Kayak", Price = 275M } ,
        //    //        { "Lifejacket"} = new Product { Name = "Lifejacket", Price = 48.95M } 
        //    //   };

        //    return View(products.Keys);
        //}



        //public ViewResult Index()
        //{

        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProduct()
        //    };

        //    Product[] prodArr =
        //    {
        //        new Product{Name = "Canoe", Price = 2000},
        //        new Product{Name = "Paddle", Price = 15}
        //    };

        //    decimal totalPrice = cart.TotalPrices();


        //    return View("Index", new string[] {$"Old Total: {totalPrice:C2}",
        //                $"New Total: {prodArr.TotalPrices():C2}"});
        //}


        //public ViewResult Index()
        //{

        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProduct()
        //    };

        //    //decimal expensiveTotal = cart.FilterByPrice(20).TotalPrices();

        //    decimal expensiveTotal = cart.Filter(FilterByPrice).TotalPrices();
        //    //decimal nameTotal = cart.FilterByName('K').TotalPrices();

        //    Func<Product, bool> nameFilter = delegate (Product p)
        //    {
        //        return p?.Name?[0] == 'S';
        //    };



        //    return View("Index", new string[] { $"Expensive Total: {expensiveTotal:C2}" });
        //}

        //lambda expressions for above indexes
        //public ViewResult Index()
        //{

        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProduct()
        //    };

        //    //replaces function "Func<>"
        //    IEnumerable<Product> fProducts = cart.Filter(p => (p?.Price ?? 0) > 20);

        //    decimal priceTotal = fProducts.TotalPrices();

        //    decimal nameTotal = cart.Filter(p => p?.Name[0] == 'K').TotalPrices();

        //    return View("Index", new string[] { $"Expensive Total: {priceTotal:C2}",
        //                            $"Name Total: {nameTotal:C2}" });
        //}


        //uses linq to grab names
        //public ViewResult Index()
        //{

        //    return View(Product.GetProduct().Select(p => p?.Name));

        //}

        //anonymous types


        public async Task<ViewResult> Index()
        {
            long? length = await MyAsyncMethods.GetPageLength();
            return View(new string[] { $"Length: {length}" });
        }
    

    }
}